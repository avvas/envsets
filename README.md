"Hands-off" scripts to assemble container templates with minimal changes.

To manage child instances via terminal, link aliases:

```
for f in *; do ln -s "$(realpath $f)" ~/.bash_aliases.d/$f; done
```

For more details, check README's in subdirectories, or refer to wiki's.

## systemd-nspawn

Namespace container, pre-configured for containerizing graphical applications.

Container configuration covers, inter alia:

- users setup at `/root/deploy.sh`
- environment variables at `.bash_profile.d`
- display socket bind at `/run/user/$uid/`
- `XDG_RUNTIME_DIR` chown service on boot

`sudo ./nspawn/create.sh` to execute the script.

## KVM (via libvirt)

`virt-install` wrapper with pre-defined VM parameters.

- `graphical` mounts an image and enables remote access using SPICE protocol.
- `console` creates a Debian-based machine with command-line interface.

`cd libvirt; ./create.sh presets/console.sh` to execute the script. Before
deploying a new `graphical` machine, link an installation image to
`~/.guests/cdrom`.

libvirt instances can also be managed in session mode daemon via
`virt-manager --connect qemu:///session`.
