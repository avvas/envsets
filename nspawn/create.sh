#!/usr/bin/env bash
set -eu -o pipefail

if false; then
declare -rA dep_cmds=(
	[]=systemd-container
	[]=debootstrap
	[sponge]=moreutils
	[mktemp]=coreutils
)
fi

join_arr() {
	local IFS="$1"; shift; echo "$*"
}

echo
exec_dir=$(dirname -- "$0")
source "$exec_dir/config/pkgs"

tmp_dir=$(mktemp -d --suffix='.nspawn')

declare -l host=${1:-john@acme}
GUEST_NAME=${host#*@}
export GUEST_USER=${host%@*}

echo "- creating container directory"
guest_dir="/var/lib/machines/$GUEST_NAME/"

test ! -d "$guest_dir"
sudo mkdir -vp -- "$guest_dir"

echo; echo "- copying container configuration"
guest_conf="/etc/systemd/nspawn/$GUEST_NAME.nspawn"
sudo mkdir -vp -- "$(dirname "$guest_conf")"

envsubst < "$exec_dir/config/unit" | sudo sponge "$guest_conf"

echo; echo "- setting up target files"
target_dir="$tmp_dir/target/"
sudo cp -r -L --preserve=mode,ownership -- "$exec_dir/target/". "$target_dir"
mv -- "$target_dir/home/user" "$target_dir/home/$GUEST_USER"

for f in \
	"${getty_host="etc/systemd/system/console-getty.service.d/override.conf"}" \
	"${getty_guest="etc/systemd/system/container-getty@.service.d/override.conf"}" \
	"${init_script="root/deploy.sh"}"
do
	f="$target_dir/$f"; envsubst '$GUEST_USER' < "$f" | sponge "$f"
done

echo $GUEST_NAME > "$target_dir/etc/hostname"
chmod +x -- "$target_dir/usr/local/bin/xdg-runtime-chown.sh"
chmod +x -- "$target_dir/$init_script"

args=(
	--arch amd64
	--variant=minbase
	--include=$(join_arr , "${pkgs[@]}")
	--components main,contrib,non-free
	unstable
)

echo; echo "- running debootstrap"
sudo debootstrap ${args[@]} "$guest_dir"

echo; echo "- copying target directory"
sudo cp -r -- "$target_dir" "$guest_dir"

echo -e "\ncontainer is deployed! to finish the installation, run:"
echo -e "sudo systemd-nspawn -a -D \"$guest_dir\" -- \"/target/$init_script\"\n"
