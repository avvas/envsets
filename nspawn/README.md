# systemd-nspawn

## X display

After container is ready, you may allow applications to use the host X display
by running the following command against the physical host:

```
xhost +local:
```

## user namespace

By default, container is running in
[user namespace](https://www.freedesktop.org/software/systemd/man/systemd-nspawn.html#-U),
if one is supported. To add the kernel attribute, create a corresponding drop-in file:

```
echo "kernel.unprivileged_userns_clone=1" | sudo tee /etc/sysctl.d/nspawn.conf
sudo sysctl kernel.unprivileged_userns_clone # to check
```

`machinectl status acme` output can be used to ensure the user namespace by
`UID Shift`, or by checking container start parameters from `supervisor` unit.

Note that these may disable access to kernel capabilities,
like `CAP_NET_ADMIN` and `CAP_NET_RAW`:
<https://github.com/systemd/systemd/issues/13128>.

`PrivateUsers=true` setting is required to in order to
"use transparent ID mapping mounts".

## socket binds

the readonly mount flag does not prevent connect()
<https://github.com/systemd/systemd/issues/7093#issuecomment-336692761>

## links

A more comprehensive guide on a similar setup is available in the following blog post:
<https://gitlab.com/uoou/LudicLinux/-/blob/master/_posts/2017-06-27-Nspawn-Steam-Container.md>.

A nice presentation with examples, compared to other tools:
<https://blog.lieter.nl/presentations/plexis-NLUUG-fall-2019-systemd-nspawn.pdf>.
