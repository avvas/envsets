#!/usr/bin/env bash
# [[ "$(systemd-detect-virt)" != "systemd-nspawn" ]] && exit 1
echo

passwd
echo "127.0.1.1 $HOSTNAME" | sudo tee -a -- /etc/hosts > /dev/null
echo "ignore the 'sudo: unable to resolve host' warning"

echo
adduser --comment "" --disabled-password $GUEST_USER
sudo usermod -a -G sudo $GUEST_USER

cp -a -- /target/. /
chown -R $GUEST_USER:$GUEST_USER -- "/home/$GUEST_USER"

find /etc/sudoers.d/ -type f -exec chmod 0440 -- {} \;
chmod 750 -- /etc/sudoers.d/

echo
systemctl enable xdg-runtime-chown

echo
