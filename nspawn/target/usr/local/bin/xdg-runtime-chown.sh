#!/bin/sh
set -e

export XDG_RUNTIME_DIR=/run/user/1000
mkdir -p -- $XDG_RUNTIME_DIR
chown 1000:1000 -- $XDG_RUNTIME_DIR # 1000:
