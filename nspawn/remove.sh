#!/usr/bin/env bash
guest_name=${1:-acme}
machinectl stop $guest_name
machinectl remove $guest_name
