# vm_name=
params+=(
	--osinfo require=off #,name=
	--cdrom ~/.guests/cdrom
	--boot uefi

	--graphics spice
	--noautoconsole

	--disk size=32

	--vcpus 6
	--ram=4096
)
