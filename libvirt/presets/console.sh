deb_url="http://deb.debian.org/debian/dists/stable/main/installer-amd64/"

pkgs+=(sudo acpi-support-base libpam-systemd)
pkgs+=(xterm bash-completion curl)

params+=(
	--location "$deb_url"
	--osinfo debiantesting
	--boot uefi

	--graphics none
	--console pty,target_type=serial
	--extra-args "console=ttyS0"

	--disk size=8

#	--vcpus 2
	--ram=1024
)
