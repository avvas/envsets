#!/usr/bin/env bash
set -eu -o pipefail

if [ $# -eq 0 ]; then
	echo "run as: \"$(basename "$0") presets/console.sh\""
	exit 1
fi

if false; then
declare -rA dep_cmds=(
	[virsh]=libvirt-clients
	[virt-viewer]=virt-viewer
	[virt-install]=virtinst
	[osinfo-query]=libosinfo-bin
	[mktemp]=coreutils
)

declare -ra dep_pkgs=(
	libvirt-daemon
	ovmf # uefi
#	libvirt-daemon-system # systemd
#	spice-client-gtk # clipboard
)
fi

echo; echo "- sourcing machine parameters"

declare -a params pkgs
params+=(--connect qemu:///session)

params+=(--osinfo detect=on)
# virt-install --osinfo list

# params+=(--disk pool=sessionpool)
# params+=(--network network=macvtap,model=virtio)
params+=(--network bridge=virbr0,model=virtio-net-pci)

source "$1"

vm_name=${vm_name:-$(basename -- "$1" .sh)}
params+=(--name=$vm_name)

echo "- deploying machine"

virt-install "${params[@]}" # exec
virsh autostart --disable $vm_name

if [[ "${params[@]}" =~ "--console" ]]; then
	echo "- copying post-installation script"
	# todo: systemd-firstboot

	# virsh destroy console
	init_script=$(mktemp)

cat << EOF > "$init_script"
apt update; apt install ${pkgs[@]}
echo "resize >/dev/null" >> /etc/bash.bashrc
EOF

	virt-copy-in -d $vm_name "$init_script" "/root/"
fi

echo "vm is deployed!"; echo
