#!/usr/bin/env bash
[ $# -eq 0 ] && exit 1

vm_name=$(basename -- "$1" .sh)

virsh destroy $vm_name
virsh managedsave-remove $vm_name
virsh undefine --remove-all-storage --nvram $vm_name
