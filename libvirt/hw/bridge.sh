#!/usr/bin/env bash
set -eu -o pipefail

# libvirt-daemon-config-network

url=https://raw.githubusercontent.com/libvirt/libvirt/master/src/network/default.xml.in
source "$(dirname "$0")/paths.sh"

path+=networks/bridge.xml
[ -f "$path" ] && exit 1

echo -e "\n creating $(basename $0 .sh) for qemu:///$qemu \n"

mkdir -p -- "$(dirname "$path")" || :;
wget -q -O "$path" -- "$url"

# status=/var/lib/libvirt/dnsmasq/virbr0.status
# sudo rm -- "$status"

# sudo ip link del virbr0
virsh --connect qemu:///$qemu net-define "$path"
