#!/usr/bin/env bash
set -eu -o pipefail

source "$(dirname "$0")/paths.sh"
type=$1 name=$2 # net or pool

for cmd in destroy undefine; do
	virsh $1-$cmd $2 || :;
done
