#!/bin/sh

if [ "$EUID" -eq 0 ]; then
	qemu=system
	path=/var/lib/libvirt/
else
	qemu=session
	path=~/.local/share/libvirt/

fi
