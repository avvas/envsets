#!/usr/bin/env bash
set -eu -o pipefail

source "$(dirname "$0")/paths.sh"

path+=images/
pool=${qemu}pool

echo -e "\n creating $(basename $0 .sh) for qemu:///$qemu at: \n $path \n"

mkdir -p -- "$path" || :;

virsh --connect qemu:///$qemu pool-define-as \
	--name $pool --type dir --target $path

virsh --connect qemu:///$qemu \
	"pool-build $pool; pool-start $pool"

# pool-autostart $pool
