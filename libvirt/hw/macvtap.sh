#!/usr/bin/env bash
set -eu -o pipefail

# echo "macvlan" | sudo tee /etc/modules-load.d/macvlan.conf
# sudo modprobe macvlan; lsmod | grep macvlan

source "$(dirname "$0")/paths.sh"

path+=networks/macvtap.xml
[ -f "$path" ] && exit 1

echo -e "\n creating $(basename $0 .sh) for qemu:///$qemu \n"

dev=$(ip -o link show | awk '{print $2}' | grep -v 'lo:' | head -n 1 | cut -d':' -f1)
mkdir -p -- "$(dirname "$path")" || :;

cat <<EOF > "$path"
<network>
	<name>default</name>
	<forward mode='bridge'>
		<interface dev='$dev'/>
	</forward>
</network>
EOF

virsh --connect qemu:///$qemu net-define "$path"
