# libvirt

[Virtualization Deployment and Administration Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/index)

## Hardware

To undefine a hardware component, run `hw/undefine.sh pool default`.

## "Operation not permitted"

Bridge/tap networks function only if created in qemu:///system space.

```
error: Failed to start network default
error: error creating bridge interface virbr0: Operation not permitted
```

## no polkit agent available to authenticate action

To temporary avoid password prompts for system mode daemon (`qemu:///system`),
add user to `libvirt` group:

```
sudo usermod -a -G libvirt $USER
getent group libvirt
```
